FROM node:latest

COPY ./public /home/public

RUN npm i -g servor

WORKDIR /home/public/javascripts/rescript-src
RUN npm i
RUN npm run build

WORKDIR /home/public/javascripts
RUN npm i
WORKDIR /home/public/javascripts/elm
RUN npx elm make --output=index.js src/Main.elm

WORKDIR /home/public
