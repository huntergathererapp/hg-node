open Belt;

/* general utilities */

let toOption = (result: Result.t<'t, string>): option<'t>
  => switch result {
  | Error(message) => { Js.Console.error(message); None }
  | Ok(t) => Some(t)
};

let toResult = (op: option<'a>, err: 'b): Result.t<'a, 'b>
  => switch op {
  | None => Result.Error(err);
  | Some(x) => Result.Ok(x);
};

let mapTogether = (first: Result.t<'a, 'error>,
                   second: Result.t<'b, 'error>,
                   func: ('a, 'b) => 'c): Result.t<'c, 'error>
  => Result.flatMap(first, f => Result.map(second, s => func(f, s)));

let getProp = (dict: Js.Dict.t<Js.Json.t>, prop: string):
  Result.t<Js.Json.t, string>
  => Js.Dict.get(dict, prop)
    -> toResult(Js.String2.concat(prop, "Parse Error: property not found: "));

let typeError = (type_: string, prop: string): string
  => "Parse Error: "
  -> Js.String2.concat(prop)
  -> Js.String2.concat(" not ")
  -> Js.String2.concat(type_)
;

let failNaN = (number: float): Result.t<float, string> => {
  if Js.Float.isNaN(number) { Result.Error("Parse Error: yielded NaN") }
    else { Result.Ok(number) }
};

let req = (t: Result.t<'t, string>,
           prop: string,
           decode: ((Js.Dict.t<Js.Json.t>, string) => Result.t<'prop, string>),
           dict: Js.Dict.t<Js.Json.t>,
           update: ('t, 'prop) => 't): Result.t<'t, string>
    => mapTogether(t, decode(dict, prop), update);

let opt = (t: Result.t<'t, string>,
           prop: string,
           decode: ((Js.Dict.t<Js.Json.t>, string) => Result.t<'prop, string>),
           dict: Js.Dict.t<Js.Json.t>,
           update: ('t, option<'prop>) => 't): Result.t<'t, string>
  => switch t {
  | Error(_) => t;
  | Ok(obj) => Result.Ok(decode(dict, prop) -> toOption |> update(obj));
};

let str = (dict: Js.Dict.t<Js.Json.t>, prop: string): Result.t<string, string>
  => getProp(dict, prop)
  -> Result.map(json => Js.Json.decodeString(json))
  -> Result.flatMap(op => toResult(op, typeError("string", prop)))
  ;

let num = (dict: Js.Dict.t<Js.Json.t>, prop: string): Result.t<float, string>
    => getProp(dict, prop)
    -> Result.map(json => Js.Json.decodeNumber(json))
    -> Result.flatMap(op => toResult(op, typeError("float", prop)));
