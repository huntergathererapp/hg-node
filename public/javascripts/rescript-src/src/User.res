type t = { username: string }

let parseUser = (str: string): option<t> =>
    Js.Json.string(str)
    -> Js.Json.decodeObject
    -> Belt.Option.flatMap(dict => dict -> Js.Dict.get("username"))
    -> Belt.Option.flatMap(json => json -> Js.Json.decodeString)
    -> Belt.Option.map(username => { username: username })
;
