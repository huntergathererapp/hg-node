/* fetch calls to the back end api */

open Fetch;

type state = { state: string };

let getOAuthState =
  (): Promise.t<Belt.Result.t<PKCE.OAuthRequest.t, string>> => {
  Fetch.withString("/api/oauth/state/")
    -> Promise.thenResolve(response => Response.clone(response))
    -> Promise.thenResolve(response => Response.json(response))
    -> Promise.thenResolve(json => Parsers.decodePkce(json));
};

let getSession = (): Promise.t<Belt.Result.t<ClientSession.t, string>> => {
  Fetch.withString("/api/state/")
    -> Promise.thenResolve(response => Response.clone(response))
    -> Promise.thenResolve(response => Response.json(response))
    -> Promise.thenResolve(json => ClientSession.parser(json))
};

let saveOAuthState = (pkce: PKCE.OAuthRequest.t): Promise.t<bool> => {
  Js.Console.log("saving oauth state...");
  Js.Console.log(pkce);
  "/api/oauth/state/"
    -> Request.Make.WithOptions.fromUrl({ ...Options.default,
                                          method: Some(#POST),
                                          body: Js.Json.stringifyAny(pkce),
                                          mode: Some(#"same-origin"),
                                          credentials: Some(#"same-origin"),
                                          cache: Some(#"no-store")
                                        })
    -> Fetch.withRequest
    -> Promise.thenResolve(resp => Response.clone(resp))
    -> Promise.thenResolve(resp => Response.ok(resp))
};

