open Belt;

let initialPkce: PKCE.OAuthRequest.t = {
  state: "",
  codeVerifier: "",
  codeChallenge: "",
  host: ""
};

let decodePkce = (json: Js.Json.t): Result.t<PKCE.OAuthRequest.t, string> =>
    switch Js.Json.classify(json) {
        | Js.Json.JSONObject(dict) => Belt.Result.Ok(initialPkce)
        -> Parsing.req("state", Parsing.str, dict, (obj, str) => {
          ...obj, state: str })
        -> Parsing.req("codeVerifier", Parsing.str, dict, (obj, str) => {
          ...obj, codeVerifier: str })
        -> Parsing.req("codeChallenge", Parsing.str, dict, (obj, str) => {
          ...obj, codeChallenge: str })
        -> Parsing.req("host", Parsing.str, dict, (obj, str) => {
          ...obj, host: str })
        | _ => Result.Error("Parse Error: not an object.");
    };

let pkce = (dict: Js.Dict.t<Js.Json.t>, prop: string):
    Result.t<PKCE.OAuthRequest.t, string>
    => Parsing.getProp(dict, prop)
    -> Result.flatMap(json => decodePkce(json))
;


      
