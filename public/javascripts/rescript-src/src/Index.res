open Elm;
open Window;

module Ports = {
  type t = {
    authenticateGitLab: Elm.sendable<string>,
    generateOAuthRequest: Elm.subscribable<unit>,
    requestGitLabToken: Elm.subscribable<unit>,
    updateOAuthRequest: Elm.sendable<PKCE.OAuthRequest.t>,
    validateState: Elm.subscribable<unit>,
  };
};

type flags = {
  user: option<User.t>
}

let app: Elm.app<Ports.t> =
  { node: None, flags: None } -> Elm.Main.initWithOptions;

app.ports.generateOAuthRequest -> Elm.subscribe(() => {
  let promise: Promise.t<PKCE.OAuthRequest.t> =
    PKCE.OAuthRequest.generate(window);
  let _ = promise -> Promise.thenResolve(req => {
    Elm.send(app.ports.updateOAuthRequest, req)
  });
  let _ = promise -> Promise.then(req => Api.saveOAuthState(req))
});

app.ports.requestGitLabToken -> Elm.subscribe(() => {
  Js.Console.log("Checking state...");
  let pkce: Promise.t<Belt.Result.t<PKCE.OAuthRequest.t, string>> =
    Api.getOAuthState();
  let actualStateResult: Promise.t<Belt.Result.t<string, string>> =
    pkce -> Promise.thenResolve(monad => {
      monad -> Belt.Result.map(req => req.state)
    });
  let sessionResult: Promise.t<Belt.Result.t<ClientSession.t, string>> =
    Api.getSession();
  let urlState: string = window -> location
      -> Location.search
      -> URLSearchParams.make
      -> URLSearchParams.get("state")
      -> Js.Nullable.toOption
      -> Belt.Option.getWithDefault("");
  let stateIsValid = actualStateResult
      -> Promise.thenResolve(result => {
        result -> Belt.Result.map(actualState => {
          Js.Console.log("Actual state: " -> Js.String2.concat(actualState));
          Js.Console.log("Expected state: " -> Js.String2.concat(urlState));
          urlState == actualState
        })
      });
  let urlCode: string = window -> location
      -> Location.search
      -> URLSearchParams.make
      -> URLSearchParams.get("code")
      -> Js.Nullable.toOption
      -> Belt.Option.getWithDefault("");
  Js.Console.log("URL Code: " -> Js.String2.concat(urlCode));
  let _ = stateIsValid
      -> Promise.then(isValid => {
        sessionResult ->
          Promise.thenResolve(sr => {
            sr -> Belt.Result.map(session => {
              let atr: GitLab.accessTokenRequest = {
                clientId: session.gitLabClientId,
                code: session.gitLabPkceOAuthRequest.codeChallenge,
                redirectUri: session.gitLabRedirectUrl,
                codeVerifier: session.gitLabPkceOAuthRequest.codeVerifier
              };
              Js.Console.log("atr: ");
              Js.Console.log(atr);

              let accessTokenPromise = GitLab.getAccessToken(atr)
                  -> Promise.thenResolve(resp => Js.Console.log(resp))
            })
          })
      });
});
