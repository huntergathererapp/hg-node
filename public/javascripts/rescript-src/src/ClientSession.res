type t = {
  gitLabClientId: string,
  gitLabPkceOAuthRequest: PKCE.OAuthRequest.t,
  gitLabRedirectUrl: string,
  username: option<string>
};

let default: t = {
  gitLabClientId: "",
  gitLabPkceOAuthRequest: Parsers.initialPkce,
  gitLabRedirectUrl: "",
  username: None
};

let parser = (json: Js.Json.t): Belt.Result.t<t, string> => {
  switch Js.Json.classify(json) {
      | Js.Json.JSONObject(dict) => Belt.Result.Ok(default)
      -> Parsing.req("gitLabClientId", Parsing.str, dict, (obj, str) => {
        ...obj, gitLabClientId: str })
      -> Parsing.req("gitLabPkceOAuthRequest", Parsers.pkce, dict,
                     (obj, pkce) => { ...obj, gitLabPkceOAuthRequest: pkce }
                    )
      -> Parsing.req("gitLabRedirectUrl", Parsing.str, dict, (obj, str) => {
        ...obj, gitLabRedirectUrl: str })
      -> Parsing.opt("username", Parsing.str, dict, (obj, opt) => {
        ...obj, username: opt })
      | _ => Belt.Result.Error("Parse Error: not an object.");
  };
}
