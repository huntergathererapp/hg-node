/* store name constants */
let pkceKey = "pkce";

let userKey = "user";

let getObj =
  (storage: Storage.Storage.t,
   storeName: string,
   parser: string => option<'obj>): option<'obj> => storage
  -> Storage.Storage.getItem(storeName)
  -> Js.Nullable.toOption
  -> Belt.Option.flatMap(str => parser(str));

let saveObj = (store:Storage.Storage.t, storeName: string, obj: 'obj):unit => {
  let _ = obj -> Js.Json.stringifyAny
      -> Belt.Option.map(str =>
        Storage.Storage.setItem(store, storeName, str)
      );
};

/*-------------------------------public----------------------------------*/

let getPkce =
  (store: Storage.Storage.t, parser: string => option<PKCE.OAuthRequest.t>):
  option<PKCE.OAuthRequest.t> =>
  getObj(store, pkceKey, parser);

let getUser = (store: Storage.Storage.t):
    option<User.t> =>
    getObj(store, userKey, User.parseUser);

let savePkce = (storage: Storage.Storage.t, pkce: PKCE.OAuthRequest.t) =>
    saveObj(storage, pkceKey, pkce);

let saveUser = (storage: Storage.Storage.t, user: User.t): unit =>
    saveObj(storage, userKey, user);


