open Fetch;

type accessToken = {
  accessToken: string,
  tokenType: string,
  expiresIn: float,
  refreshToken: string,
  createdAt: float
};

type accessTokenRequest = {
  clientId: string,
  code: string,
  redirectUri: string,
  codeVerifier: string
};

module Private = {
  type accessTokenRequestJson = {
    client_id: string,
    code: string,
    grant_type: string,
    redirect_uri: string,
    code_verifier: string
  };

  let accessTokenRequestToJsonString = (atr: accessTokenRequest):
      option<string> => {
    let jsonable: accessTokenRequestJson = {
      client_id: atr.clientId,
      code: atr.code,
      grant_type: "authorization_code",
      redirect_uri: atr.redirectUri,
      code_verifier: atr.codeVerifier
    };
    Js.Json.stringifyAny(jsonable);
  };

  let initialAccessToken = {
    accessToken: "",
    tokenType: "",
    expiresIn: 0.0,
    refreshToken: "",
    createdAt: 0.0
  };

  let parseAccessToken = (json: Js.Json.t): Belt.Result.t<accessToken, string>
      => {
    switch Js.Json.classify(json) {
        | Js.Json.JSONObject(dict) => Belt.Result.Ok(initialAccessToken)
        -> Parsing.req("access_token", Parsing.str, dict, (obj, str) => {
          ...obj, accessToken: str })
        -> Parsing.req("token_type", Parsing.str, dict, (obj, str) => {
          ...obj, tokenType: str })
        -> Parsing.req("expires_in", Parsing.num, dict, (obj, n) => {
          ...obj, expiresIn: n })
        -> Parsing.req("refresh_token", Parsing.str, dict, (obj, str) => {
          ...obj, refreshToken: str })
        -> Parsing.req("created_at", Parsing.num, dict, (obj, n) => {
          ...obj, createdAt: n })
        | _ => Belt.Result.Error("Parse Error: not an object.");
    }; /* end switch */
  }; /* end function */
}; /* end Private module */


let getAccessToken = (tokenRequest: accessTokenRequest):
    Promise.t<Belt.Result.t<accessToken, string>> => {
      "https://gitlab.com/oauth/token"
        -> Request.Make.WithOptions
        .fromUrl({...Options.default,
                  method: Some(#POST),
                  body: Private.accessTokenRequestToJsonString(tokenRequest),
                  cache: Some(#"no-store")
                 })
        -> Fetch.withRequest
        -> Promise.thenResolve(response => response -> Response.json)
        -> Promise.thenResolve(json => Private.parseAccessToken(json))
    };
