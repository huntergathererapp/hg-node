@val external window: Dom.window = "window";

module Location = {
  type t;
  @get external search: t => string = "search";
};

@get external location: Dom.window => Location.t = "location";

module URLSearchParams = {
  type t;
  @send external get: (t, string) => Js.Nullable.t<string> = "get";
  @new external make: string => t = "URLSearchParams";
};
