module Flags exposing (Flags, decoder, init)

import Json.Decode
import Json.Decode.Pipeline 
import Models.User

type alias Flags =
    { {-baseUrl : String
    ,-} user : Maybe Models.User.User
    }

decoder : Json.Decode.Decoder Flags
decoder =
    Json.Decode.succeed Flags
--        |> Json.Decode.Pipeline.optional "baseUrl" Json.Decode.string ""
        |> Json.Decode.Pipeline.optional
           "user"
           (Json.Decode.map Just Models.User.decodeUser)
           Nothing

init : Flags
init = { {-baseUrl = "",-} user = Nothing }
