module Routes exposing (Route(..), parseUrl)

import Url
import Url.Parser exposing ((</>), (<?>))
import Url.Parser.Query

type Route =
    Contribute
        | Home
        | Maintain
        | RedirectGitLab
        | SignIn
        | SignUp
        | NotFound

parseUrl : Url.Url -> Route
parseUrl url =
    case Url.Parser.parse matchRoute url of
        Just route ->
            route
        Nothing ->
            NotFound

---------------------private--------------------
                
matchRoute : Url.Parser.Parser (Route -> a) a
matchRoute =
    Url.Parser.oneOf
        [ Url.Parser.map Home Url.Parser.top
        , Url.Parser.map Contribute (Url.Parser.s "contribute")
        , Url.Parser.map Maintain (Url.Parser.s "maintain")
        , Url.Parser.map RedirectGitLab
             (Url.Parser.s "redirect" </> Url.Parser.s "gitlab")
        , Url.Parser.map SignIn (Url.Parser.s "signin")
        , Url.Parser.map SignUp (Url.Parser.s "signup")
        ]

 
