module Views.Home exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Model
import Msg

root: Model.Model -> Html Msg.Msg
root model =
    case model.session of
        Just session ->
            case session.username of
                Just username -> authenticated
                Nothing -> unauthenticated
        Nothing -> unauthenticated
-------------------------------------------------------------------------------

authenticated: Html Msg.Msg
authenticated =
    main_ [ ]
        [ div [ class "bubble-neutral" ]
              [ h2 [ class "h2" ]
                    [ a [ href "/maintain" ] [ text "Maintain" ] ]
              , p [ ]
                  [ text  "Import your projects and make governance "
                  , text "decisions."
                  ]
              ]
        , div [ class "bubble-neutral" ]
              [ h2 [ class "h2" ]
                    [ a [ href "/contribute" ] [ text "Contribute" ] ]
              , p [ ]
                  [ text  "Search for projects, donate, and more." 
                  ]
              ]
        ]

unauthenticated : Html Msg.Msg
unauthenticated =
    main_ [ ]
        [ div [ class "bubble-neutral" ]
              [ h2 [ class "h2" ] [ a [ href "/signin" ] [ text "Sign in" ] ] 
              , p [ ]
                  [ text "Check in on your investments, make withdrawls, and "
                  , text "pay it forward."
                  ]
              ]
        , div [ class "bubble-neutral" ]
              [ h2 [ class "h2" ] [ a [ href "/signup" ] [ text "Sign up" ] ]
              , p [ ]
                  [ text "Invest in your projects' future, contribute to "
                  , text "others, and get rewarded for your hard work. "
                  ]
              ]
        ]
