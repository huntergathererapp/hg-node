module Views.Maintain exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Msg
import Model
import Models.ClientSession
import Models.MaintainModel
import Url.Builder

root : Model.Model -> Html Msg.Msg
root model =
    case model.maintain.oAuthRequest of
        Just req ->
            case model.session of
                Just session -> requestLoaded session req
                Nothing -> loading
        Nothing -> loading
            

--private methods

loading : Html Msg.Msg
loading = 
    main_ [ ]
        [ div [ class "bubble-neutral" ]
              [ text "Loading..." ]
        ]
                
requestLoaded : Models.ClientSession.ClientSession
              -> Models.MaintainModel.OAuthRequest
              -> Html Msg.Msg
requestLoaded session req = 
        main_ [ ]
        [ div [ class "bubble-neutral" ]
              [ h2 [ class "h2" ] [ text "GitLab" ]
              , p [ ]
                  [ text "Please "
                  , a [ href (gitLabLink session req) ]
                      [ text "sign in to GitLab" ]
                  , text " to manage your GitLab projects."
                  ]
              ]
        ]

gitLabLink : Models.ClientSession.ClientSession
           -> Models.MaintainModel.OAuthRequest
           -> String
gitLabLink session req = 
    Url.Builder.crossOrigin "https://gitlab.com"
         [ "oauth", "authorize" ]
         [ Url.Builder.string "client_id" session.gitLabClientId
         , Url.Builder.string "redirect_uri"
             (req.host ++ session.gitLabRedirectUrl)
         , Url.Builder.string "response_type" "code"
         , Url.Builder.string "state" req.state
         , Url.Builder.string "scope" scope
         , Url.Builder.string "code_challenge" req.codeChallenge
         , Url.Builder.string "code_challenge_method" "S256"
         ]

{- gitLabAppId =
    "eafb14541636557680f9b2423bb4d2ebd55c3a5dc7f7abc7b5d6ffcba64ca6ce"

gitLabRedirect = "/redirect/gitlab"
-}

scope = "read_api read_user"
