module Views.SignIn exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Msg
import Models.SignInModel exposing (SignInModel)
import ValidationRule
import Views.ValidationRule

root : Models.SignInModel.SignInModel -> Html Msg.Msg
root model =
    Html.form [ class "bubble-neutral", onSubmit Msg.SignInSubmit ]
        [ h2 [ ] [ text "Sign In" ]
        , div [ class "flex-row" ]
            [ label [ class "label-input-text", for "username" ]
                  [ text "username" ]
            , input [ class "input-text"
                    , name "username"
                    --match alphanumerics, underscores, and hyphens
                    , onInput Msg.SignInUpdateUsername
                    , pattern "[\\w-]{1,128}"
                    , placeholder "username"
                    , required True
                    , type_ "text"
                    , value model.username
                    ] [ ]
            ]
        , div [ class "flex-row" ]
            [ label [ class "label-input-text", for "password" ]
                  [ text "password" ]
            , input [ class "input-text"
                    , minlength 16
                    , maxlength 128
                    , name "password"
                    , onInput Msg.SignInUpdatePassword
                    , placeholder "password"
                    , required True
                    , type_ "password"
                    , value model.password
                    ][ ]
            ]
        , label [ ]
            [ input [ checked model.agreeToTerms
                    , onCheck Msg.SignInUpdateAgreeToTerms
                    , name "agreeToTerms"
                    , type_ "checkbox" ] []
            , text " I affirm that I will not be a jerk on this website."
            ]
        , Views.ValidationRule.root Models.SignInModel.validationRules model
        , submission Models.SignInModel.validationRules model
        ]
                
            
-------------------------------------------------------------------------------

submission : List (ValidationRule.ValidationRule SignInModel)
             -> SignInModel
             -> Html Msg.Msg
submission rules model =
    if ValidationRule.isValid rules model
    then input [ class "input-button"
               , type_ "submit"
               , value "sign in"
               ] [ ]
            
    else text ""

