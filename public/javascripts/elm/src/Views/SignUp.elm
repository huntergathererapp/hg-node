module Views.SignUp exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model
import Models.RegisterModel exposing (RegisterModel)
import Msg
import ValidationRule
import Views.ValidationRule

root: Models.RegisterModel.RegisterModel -> Html Msg.Msg
root model =
    Html.form [ class "bubble-neutral", onSubmit Msg.SignUpSubmit ]
        [ h2 [ ] [ text "Sign up" ]
        , div [ class "flex-row" ]
            [ label [ class "label-input-text", for "username" ]
                  [ text "username"
                  ]
            , input [ class "input-text"
                    , name "username"
                    --match alphanumerics, underscores, and hyphens
                    , onInput Msg.SignUpUpdateUsername 
                    , pattern "[\\w-]{1,128}"
                    , placeholder "username"
                    , required True
                    , type_ "text"
                    , value model.username
                    ] [ ]
            ]--end div
        , div [ class "flex-row" ]
            [ label [ class "label-input-text", for "password" ]
                  [ text "password" ]
            , input [ class "input-text"
                    , minlength 16
                    , maxlength 128
                    , name "password"
                    , onInput Msg.SignUpUpdatePassword
                    , placeholder "password"
                    , required True
                    , type_ "password"
                    , value model.password
                    ][ ]
            ]
        , div [ class "flex-row" ]
            [ label [ class "label-input-text", for "confirmPassword" ]
                  [ text "confirm" ]
            , input [ class "input-text"
                    , placeholder "confirm password"
                    , name "confirmPassword"
                    , onInput Msg.SignUpUpdateConfirmPassword
                    , type_ "password"
                    , value model.confirmPassword
                    ] []
            ]
        , label [ ]
            [ input [ checked model.agreeToTerms
                    , onCheck Msg.SignUpUpdateAgreeToTerms
                    , name "agreeToTerms"
                    , type_ "checkbox" ] []
            , text " I affirm that I will not be a jerk on this website."
            ]
        , Views.ValidationRule.root Models.RegisterModel.validationRules model
        , submission Models.RegisterModel.validationRules model
        ]
                
            
-------------------------------------------------------------------------------

submission : List (ValidationRule.ValidationRule RegisterModel)
             -> RegisterModel
             -> Html Msg.Msg
submission rules model =
    if ValidationRule.isValid rules model
    then input [ class "input-button"
               , type_ "submit"
               , value "sign up"
               ] [ ]
            
    else text ""
