module Views.ValidationRule exposing (root)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Model
import Msg
import ValidationRule

--takes a list of ValidationRules and a model to validate
--returns an HTML fragment representing an explanation of validation rules
--for user. 
root : List (ValidationRule.ValidationRule model) -> model -> Html Msg.Msg
root rules model =
    ul [ class "ul-validation" ] (rules |> (List.map (\r -> ruleItem r model)))

-------------------------------------------------------------------------------

invalid : ValidationRule.ValidationRule model-> Html Msg.Msg
invalid rule =
    li [ class "ul-validation-li-invalid" ]
        [ text "❔ "
        , text rule.displayText
        ]

ruleItem : ValidationRule.ValidationRule model -> model -> Html Msg.Msg
ruleItem rule m =
    if rule.alwaysDisplay
    then if rule.fn(m) then valid rule else invalid rule
    else if rule.fn(m) then text "" else invalid rule
        
valid : ValidationRule.ValidationRule model -> Html Msg.Msg
valid rule =
    li [ class "ul-validation-li-valid" ]
        [ text "✔️ "
        , text rule.displayText
        ]
        
