module Views.Main exposing (root)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model
import Msg
import Routes
import Url
import Views.Home
import Views.Maintain
import Views.SignIn
import Views.SignUp

root : Model.Model -> Browser.Document Msg.Msg
root model =
  { title = "Hunter / Gatherer | bountiful project funding"
  , body =
      [ div [ class "container" ]
            [ div [ class "container-inset" ]
                  [ div [ class "container-scrolling-area" ]
                        [ div [ class "container-scrolling-content" ]
                              [ header model
                              , routeView model
                              , footer model
                              ]
                        ]
                  ]
            ]
      ]     
  }
    
--------------------------------------

footer: Model.Model -> Html Msg.Msg
footer model =
    case model.error of
        Just error ->
            div [ class "bubble-warn" ]
                [ div [ class "flex-row" ]
                      [ p [ class "flex-1" ] [ text error ]
                      , span []
                          [ input [ onClick (Msg.ClearError )
                                  , title "close error"
                                  , type_ "button"
                                  , value "✖️" ] []
                          ]
                      ]
                ]
        Nothing -> text ""

header: Model.Model -> Html Msg.Msg
header model =
    div [ class "bubble-neutral header" ]
        [ h1 [ class "h1" ] [ text "Hunter / Gatherer" ]
        , navigation model 
        ]

navigation: Model.Model -> Html Msg.Msg
navigation model =
    case Model.authenticated model of
        Just user ->
            nav [ class "flex-row" ]
                [ a [ class "nav-a", href "/contribute" ]
                      [ text "contribute" ]
                , a [ class "nav-a", href "/maintain" ]
                    [ text "maintain" ]
                ]
        Nothing -> 
            nav [ class "flex-row" ]
                [ a [ class "nav-a", href "/signin" ]
                      [ text "sign in" ]
                , a [ class "nav-a", href "/signup" ]
                    [ text "sign up" ]
                ]

routeView : Model.Model -> Html Msg.Msg
routeView model =
    case model.route of
        Routes.Home -> Views.Home.root model
        Routes.Maintain -> Views.Maintain.root model
        Routes.RedirectGitLab -> h2 [] [ text "Redirecting..." ] 
        Routes.SignUp -> Views.SignUp.root model.register
        Routes.SignIn -> Views.SignIn.root model.signIn
        Routes.NotFound -> h2 [] [ text "Not found." ]
        _ -> h1 [ ] [ text "Watch this space." ]
