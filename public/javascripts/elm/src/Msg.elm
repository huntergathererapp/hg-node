module Msg exposing (..)

import Browser
import Http
import Json.Encode
import Models.ClientSession
import Models.User
import Url

type Msg
    = ClearError --String {-string ignored -}
    | GotSession (Result Http.Error Models.ClientSession.ClientSession)
    | LinkClicked Browser.UrlRequest
    | MaintainUpdateOAuthRequest Json.Encode.Value
    | Nothing (Result Http.Error ())
    | SignInGotSubmission (Result Http.Error Models.User.User)
    | SignInSubmit
    | SignInUpdateAgreeToTerms Bool
    | SignInUpdatePassword String
    | SignInUpdateUsername String
    | SignUpGotSubmission (Result Http.Error Models.User.User)
    | SignUpSubmit
    | SignUpUpdateAgreeToTerms Bool
    | SignUpUpdateConfirmPassword String
    | SignUpUpdatePassword String
    | SignUpUpdateUsername String
    | UrlChanged Url.Url
      
