module Submit exposing (getSession, signIn, signUp)

import Http
import Json.Encode
import Models.ClientSession
import Models.MaintainModel
import Models.RegisterModel
import Models.SignInModel
import Models.User
import Msg

getSession : Cmd Msg.Msg
getSession =
    Http.get { url = "/api/state/"
             , expect = Http.expectJson
                        Msg.GotSession Models.ClientSession.decode
             }

signIn : Models.SignInModel.SignInModel -> Cmd Msg.Msg
signIn model =
    Http.post
        { url = "/api/signin/"
        , body = Http.jsonBody
                 ( Json.Encode.object
                       [ ("username", Json.Encode.string model.username )
                       , ("password", Json.Encode.string model.password )
                       , ("agreeToTerms", Json.Encode.bool model.agreeToTerms )
                       ]
                 )
        , expect = Http.expectJson
                   Msg.SignInGotSubmission
                       Models.User.decodeUser
        }
    

signUp : Models.RegisterModel.RegisterModel -> Cmd Msg.Msg
signUp model =
    Http.post
        { url = "/api/signup/"
        , body = Http.jsonBody
                 ( Json.Encode.object
                       [ ("username", Json.Encode.string model.username )
                       , ("password", Json.Encode.string model.password )
                       , ("confirmPassword"
                         , Json.Encode.string model.confirmPassword
                         )
                       , ("agreeToTerms", Json.Encode.bool model.agreeToTerms )
                       ]
                 )
        , expect = Http.expectJson
                   Msg.SignUpGotSubmission
                       Models.User.decodeUser
        }
