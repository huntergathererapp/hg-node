module ValidationRule exposing (isValid, ValidationRule)

type alias ValidationRule model =
    { alwaysDisplay: Bool
    , displayText: String
    , fn: (model -> Bool)
    }


isValid : List (ValidationRule model) -> model -> Bool
isValid rules m =
    rules |> List.filter (\r -> not (r.fn m)) |> List.isEmpty

