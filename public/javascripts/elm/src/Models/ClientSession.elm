{- corresponds to web.models.ClientSession on the back end. Not a part of the 
   front end model but its component parts are parsed into the front end model
   appropriately. 
-}
module Models.ClientSession exposing (ClientSession, decode)

import Json.Decode
import Json.Decode.Pipeline
import Models.GitLabOAuthResponse

type alias ClientSession =
    { gitLabClientId : String
    , gitLabOAuthResponse : Maybe
                            Models.GitLabOAuthResponse.GitLabOAuthResponse
    , gitLabRedirectUrl : String
    , username : Maybe String
    }
    
decode : Json.Decode.Decoder ClientSession
decode =
    Json.Decode.succeed ClientSession
        |> Json.Decode.Pipeline.required "gitLabClientId" Json.Decode.string
        |> Json.Decode.Pipeline.optional "gitLabOAuthResponse"
           (Json.Decode.map Just Models.GitLabOAuthResponse.decode) Nothing
        |> Json.Decode.Pipeline.required "gitLabRedirectUrl" Json.Decode.string
        |> Json.Decode.Pipeline.optional "username"
           (Json.Decode.map Just Json.Decode.string) Nothing
 
