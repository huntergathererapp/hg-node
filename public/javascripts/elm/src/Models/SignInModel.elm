module Models.SignInModel exposing (init, SignInModel, validationRules)

import Regex
import ValidationRule

type alias SignInModel =
    { username : String
    , password : String
    , agreeToTerms: Bool
    }

init : SignInModel
init =
    { username=""
    , password=""
    , agreeToTerms=False
    }    

validationRules : List (ValidationRule.ValidationRule SignInModel)
validationRules =
    [ { alwaysDisplay = True
      , displayText = "A username is required."
      , fn = \model -> (String.length model.username) > 0
      }
    , { alwaysDisplay = True
      , displayText = "Usernames may contain alphanumeric characters, hyphens,"
                      ++ " and underscores."
      , fn = \model -> "[^\\w-]"
      |> Regex.fromString
      |> Maybe.withDefault Regex.never
      |> \regex -> not (Regex.contains regex model.username)
      }
    , { alwaysDisplay = True
      , displayText = "Please agree to the terms of service."
      , fn = \model -> model.agreeToTerms
      }
    , { alwaysDisplay = False
      , displayText = "Usernames may be no more than 128 characters."
      , fn = \model -> (String.length model.username) <= 128
      }
    , { alwaysDisplay = False
      , displayText = "Passwords may be no more than 128 characters."
      , fn = \model -> (String.length model.password) <= 128
      }
    ]
