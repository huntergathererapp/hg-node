module Models.GitLabOAuthResponse exposing (GitLabOAuthResponse, decode)

import Json.Decode
import Json.Decode.Pipeline 

type alias GitLabOAuthResponse =
    { accessToken : String
    , createdAt : Int
    , expiresIn : Int
    , refreshToken : String
    }

decode : Json.Decode.Decoder GitLabOAuthResponse
decode =
    Json.Decode.succeed GitLabOAuthResponse
        |> Json.Decode.Pipeline.required "access_token" Json.Decode.string
        |> Json.Decode.Pipeline.required "created_at" Json.Decode.int
        |> Json.Decode.Pipeline.required "expires_in" Json.Decode.int
        |> Json.Decode.Pipeline.required "refresh_token" Json.Decode.string
