module Models.User exposing (decodeUser, User)

import Json.Decode
import Json.Decode.Pipeline 

type alias User =
    { username: String }

decodeUser : Json.Decode.Decoder User
decodeUser =
    Json.Decode.succeed User
        |> Json.Decode.Pipeline.required "username" Json.Decode.string
