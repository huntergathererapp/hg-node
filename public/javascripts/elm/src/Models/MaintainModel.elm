module Models.MaintainModel
    exposing (decodeOAuthRequest, init, MaintainModel, OAuthRequest)

import Json.Decode
import Json.Decode.Pipeline 

type alias MaintainModel =
    { oAuthRequest : Maybe OAuthRequest
    }

type alias OAuthRequest =
    { state: String
    , codeVerifier: String
    , codeChallenge: String
    , host: String
    }

decodeOAuthRequest : Json.Decode.Decoder OAuthRequest
decodeOAuthRequest =
    Json.Decode.succeed OAuthRequest
        |> Json.Decode.Pipeline.required "state" Json.Decode.string
        |> Json.Decode.Pipeline.required "codeVerifier" Json.Decode.string
        |> Json.Decode.Pipeline.required "codeChallenge" Json.Decode.string
        |> Json.Decode.Pipeline.required "host" Json.Decode.string

init : MaintainModel
init =
    { oAuthRequest = Nothing
    }
