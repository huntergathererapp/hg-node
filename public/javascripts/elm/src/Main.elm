module Main exposing (main)

import Action
import Browser
import Browser.Navigation as Nav
import Flags
import Json.Decode as Decode
import Model exposing (Model)
import Models.User
import Models.MaintainModel as MaintainModel
import Msg exposing (..)
import Ports
import Routes
import Submit
import Url
import Views.Main

-- MAIN

main : Program Decode.Value Model Msg
main =
  Browser.application
    { init = init
    , view = Views.Main.root
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }

init : Decode.Value -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init value url key =
    case Decode.decodeValue Flags.decoder value of
        Ok flags ->
            ( Model.init flags key url
            , Cmd.batch [ Action.match (Routes.parseUrl url)
                        , Submit.getSession
                        ]
            )
        Err _ ->
            ( Model.init Flags.init key url
            , Cmd.batch [ Action.match (Routes.parseUrl url)
                        , Submit.getSession
                        ]
            )

-- UPDATE

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
      ClearError -> ({ model | error = Maybe.Nothing }, Cmd.none)
      GotSession result ->
          case result of
              Ok session -> (Model.withSession model session, Cmd.none)
              Err error -> (Model.setHttpError model error, Cmd.none) 
      LinkClicked urlRequest ->
          case urlRequest of
              Browser.Internal url ->
                  ( model, Nav.pushUrl model.key (Url.toString url) )
              Browser.External href -> ( model, Nav.load href )
      MaintainUpdateOAuthRequest value ->
          case Decode.decodeValue MaintainModel.decodeOAuthRequest value of
              Ok oAuthRequest -> model.maintain
                 |> \m -> { m | oAuthRequest = Just oAuthRequest }
                 |> \maintain -> ({model | maintain = maintain }, Cmd.none)
              Err error -> (model, Cmd.none)--TODO Decode.Error handling
      Msg.Nothing _ -> ( model, Cmd.none )
      SignInGotSubmission result ->
          case result of
              Ok user -> (model, Cmd.batch
                                    [ Nav.pushUrl model.key "/"
                                    , Submit.getSession
                                    ])
              Err err ->
                  "Sign in failed. Please check your username and password."
                      |> \error -> ({ model | error = Just error }, Cmd.none)
      SignInSubmit -> (model, Submit.signIn model.signIn)
      SignInUpdateAgreeToTerms value ->
          model.signIn |> \signIn -> { signIn | agreeToTerms = value}
                       |> \s -> ({ model | signIn = s }, Cmd.none)
      SignInUpdatePassword password ->
          model.signIn |> \signIn -> { signIn | password = password }
                       |> \s -> ({ model | signIn = s }, Cmd.none)
      SignInUpdateUsername username ->
          model.signIn |> \signIn -> { signIn | username = username }
                       |> \s -> ({ model | signIn = s }, Cmd.none)
      SignUpGotSubmission result ->
          case result of
              Ok user -> (model, Cmd.batch
                                    [ Nav.pushUrl model.key "/"
                                    , Submit.getSession
                                    ])
              Err error -> (Model.setHttpError model error, Cmd.none) 
      SignUpUpdateAgreeToTerms value ->
          model.register |> \register -> { register | agreeToTerms = value}
                         |> \r -> ({ model | register = r }, Cmd.none)
      SignUpUpdateConfirmPassword confirmation ->
          model.register |> \r -> { r | confirmPassword = confirmation }
                         |> \register -> { model | register = register }
                         |> \m -> (m, Cmd.none)
      SignUpUpdatePassword password ->
          model.register |> \register -> { register | password = password }
                         |> \r -> ({ model | register = r }, Cmd.none)
      SignUpUpdateUsername username ->
          model.register |> \register -> { register | username = username }
                         |> \r -> ({ model | register = r }, Cmd.none)
      SignUpSubmit -> (model, Submit.signUp model.register)
      {-UpdateUser json ->
          case Decode.decodeValue Models.User.decodeUser json of
              Ok user -> ({ model | user = Just user }, Cmd.none)
              Err message -> (model, Cmd.none) --TODO
       -}
      UrlChanged url ->
          Routes.parseUrl url
              |> \r -> ({ model | route = r }, Action.match r)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.batch [ Ports.updateOAuthRequest MaintainUpdateOAuthRequest
            --, Ports.updateUser UpdateUser
            ]



