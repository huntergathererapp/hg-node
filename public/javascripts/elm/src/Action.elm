module Action exposing (match)

import Model
import Msg
import Ports
import Routes exposing (Route(..))

match : Routes.Route -> Cmd Msg.Msg
match route  =
    case route of
        Maintain -> Ports.generateOAuthRequest()
        RedirectGitLab -> Ports.requestGitLabToken()
        _ -> Cmd.none
        
