module Model exposing (HomeModel, init, Model, authenticated, setHttpError,
                           updateUrl, withSession)

import Browser.Navigation
import Flags
import Http
import Models.ClientSession
import Models.MaintainModel
import Models.RegisterModel
import Models.SignInModel
import Models.User
import Routes
import Url
import Url.Parser


type alias HomeModel =
    { placeholder : String }    


type alias Model =
    { error : Maybe String
    , home : HomeModel
    , key : Browser.Navigation.Key
    , maintain : Models.MaintainModel.MaintainModel
    , register : Models.RegisterModel.RegisterModel
    , route : Routes.Route
    , session : Maybe Models.ClientSession.ClientSession
    , signIn : Models.SignInModel.SignInModel
    , url : Url.Url
    --, user : Maybe Models.User.User
    }
    
init: Flags.Flags -> Browser.Navigation.Key -> Url.Url -> Model
init flags key url= 
    { error = Nothing
    , home = HomeModel ""
    , key = key
    , maintain = Models.MaintainModel.init
    , register = Models.RegisterModel.init
    , route = Routes.parseUrl url
    , session = Nothing
    , signIn = Models.SignInModel.init
    , url = url
    --, user = Nothing
    }

authenticated: Model -> Maybe String
authenticated model =
    case model.session of
        Just session ->
            case session.username of
                Just username -> Just username
                Nothing -> Nothing
        Nothing -> Nothing
    
setHttpError: Model -> Http.Error -> Model
setHttpError model err =
    { model | error = Just (httpErrorToString err) }


httpErrorToString: Http.Error -> String
httpErrorToString err =
    case err of
        Http.BadBody message -> "Error while parsing: " ++ message
        Http.BadStatus code -> "Bad Status: " ++ String.fromInt code
        Http.BadUrl str ->  "Bad URL: " ++ str
        Http.NetworkError -> "Network error: please check your connection."
        Http.Timeout -> "Network timed out."

updateUrl: Model -> Url.Url -> Model
updateUrl m url =
    { m | route = Routes.parseUrl url
    , url = url
    }

withSession: Model -> Models.ClientSession.ClientSession -> Model
withSession model session =
    { model | session = Just session
    }
