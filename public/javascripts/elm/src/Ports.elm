port module Ports exposing (..)

import Json.Decode
import Models.User

{---------------------------outgoing-----------------------------------------}
port generateOAuthRequest : (() -> Cmd msg)

port requestGitLabToken : (() -> Cmd msg)
                            
port saveUser : Models.User.User -> Cmd msg

port validateState : (() -> Cmd msg)

{---------------------------incoming------------------------------------------}
port authenticateGitLab: (String -> msg) -> Sub msg

port updateOAuthRequest : (Json.Decode.Value -> msg) -> Sub msg

port updateUser : (Json.Decode.Value -> msg) -> Sub msg
